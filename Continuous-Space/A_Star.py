from pygame.event import get
from pygame.mouse import get_pos, get_pressed
from pygame.display import flip
from pygame.locals import K_SPACE, K_ESCAPE, KEYDOWN, QUIT, K_0, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9, K_r, K_q
from numpy.random import randint
from numpy import infty, array, unique
from math import sqrt
from queue import PriorityQueue
from constants import *


# A_Star is an algorithm based on Dijkstra's Algorithm with a heuristic that
# tells us how to choose which node in the open set is most worth checking by
# utilizing a function that guesses how far away from the end each node is
# in order to do a "best-first" search
# https://en.wikipedia.org/wiki/A*_search_algorithm
def A_Star(adjacency_matrix, cost_matrix, nodes, area):
    # This returns the current quickest path from BEGINNING to the current pos
    def reconstruct_path(camefrom, cur, nodes):
        cur_index = nodes.index(cur)
        if cur != END and cur != BEGINNING:
            total_path = [cur]
        else:
            total_path = []
        while camefrom[cur_index] != None:
            cur_index = camefrom[cur_index]
            cur = nodes[cur_index]
            if cur != END and cur != BEGINNING:
                total_path.insert(0, cur)
        return total_path

    # Returns which neighbors are viable
    def neighbor_getter(pos, adjacency_matrix, nodes):
        index = nodes.index(pos)
        legal_moves = []
        for i in range(adjacency_matrix[index].shape[0]):
            if adjacency_matrix[index, i] == 1:
                legal_moves.append(nodes[i])
        return legal_moves

    # function to approximate distance to goal
    h = lambda cur_pos: area.dist(cur_pos, END)

    # keep track of when we update nodes
    count = 0

    # PriorityQueue is an efficient data structure for this and will hold
    # all nodes that are being used
    open_set = PriorityQueue()
    open_set.put((0, -count, BEGINNING))

    # stores whether point [i,j] is currently in the PriorityQueue, which can't
    # efficiently do this
    in_open_set = [False for j in range(len(nodes))]
    in_open_set[nodes.index(BEGINNING)] = True

    # Stores the previous node in the best route to point [i,j]
    came_from = [None for j in range(len(nodes))]

    # g_score contains the best current cost to get to point [i,j]
    g_score = [infty for j in range(len(nodes))]
    g_score[nodes.index(BEGINNING)] = 0

    # f_score contains the best current estimate to get from beginning to end
    # when going through point [i,j]
    f_score = [0 for j in range(len(nodes))]
    f_score[nodes.index(BEGINNING)] = h(BEGINNING)

    running = True
    solving = True
    solved = False
    prev_best_path_guess = [] # for showing current best path
    while running:
        if solving:
            if open_set.empty():
                print('There is no path from beginning to end yet.')
                solving = False
                return False

            current = open_set.get()[2]
            cur_index = nodes.index(current)
            in_open_set[cur_index] = False

            if current == END:
                move_list = reconstruct_path(came_from, current, nodes)
                solving = False
                solved = True
                area.draw_astar_solution(move_list[0], BEGINNING, color=GREEN)
                flip()
                for i in range(len(move_list)-1):
                    area.draw_astar_solution(move_list[i], move_list[i+1], color=GREEN)
                    flip()
                area.draw_astar_solution(move_list[-1], END, color=GREEN)
                flip()
                return True

            neighbors = neighbor_getter(current, adjacency_matrix, nodes)
            for neighbor in neighbors:
                n_index = nodes.index(neighbor)
                temp_g_score = g_score[nodes.index(current)] + cost_matrix[cur_index, n_index]
                # if current best cost to neighbor is cheapest thus far
                if temp_g_score < g_score[n_index]:
                    came_from[n_index] = cur_index
                    g_score[n_index] = temp_g_score
                    f_score[n_index] = temp_g_score + h(neighbor)
                    if not in_open_set[n_index]:
                        count += 1
                        open_set.put((f_score[n_index], -count, neighbor))
                        in_open_set[n_index] = True

        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    stop_searching = not stop_searching

                elif event.key == K_ESCAPE:
                    running = False
                    return False

                elif event.key == K_r:
                    return True

    return False
