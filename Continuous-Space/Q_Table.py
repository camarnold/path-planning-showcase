from constants import WIDTH, HEIGHT, BEGINNING, END
from numpy import array, linspace, zeros, argmax, log10, infty
from numpy.linalg import norm
from numpy.random import uniform, randint

def Q_Table():

    def reward_getter(i, j):
        return -log10(norm((width_array[i] - end_pos[0], height_array[j] - end_pos[1])))/10

    def get_action(Q, s, epsilon):
        i,j = s
        if uniform(0, 1) < epsilon:
            while True:
                a = randint(0,4)
                if i == 0 and a == 2:
                    continue
                elif i == width_array.shape[0]-1 and a == 0:
                    continue
                elif j == 0 and a == 3:
                    continue
                elif j == height_array.shape[0]-1 and a == 1:
                    continue
                else:
                    break
            return a
        counter = 0
        while True:
            a = argmax(Q[i, j])
            counter += 1
            if i == 0 and a == 2:
                Q[i,j,a] = Q.min()-.01
            elif i == width_array.shape[0]-1 and a == 0:
                Q[i,j,a] = Q.min()-.01
            elif j == 0 and a == 3:
                Q[i,j,a] = Q.min()-.01
            elif j == height_array.shape[0]-1 and a == 1:
                Q[i,j,a] = Q.min()-.01
            else:
                break
        return argmax(Q[i, j])

    fineness = 10
    epsilon = 1
    gamma = 0.98
    alpha = 0.1
    trials = 50000
    max_steps = 1000
    threshold = 1e-3

    MOVES = [(1,0), (0,1), (-1,0), (0,-1)]
    begin_pos = (BEGINNING[0]//fineness, BEGINNING[1]//fineness)
    end_pos = (END[0]//fineness, END[1]//fineness)


    width_array = linspace(0, WIDTH, WIDTH//fineness)
    height_array = linspace(0, HEIGHT, HEIGHT//fineness)

    Q = zeros((WIDTH//fineness, HEIGHT//fineness, 4))
    # Q = uniform(-1,1,size=(WIDTH//fineness, HEIGHT//fineness, 4))
    # Q = array([[[reward_getter(i,j) for a in range(4)] for j in range(HEIGHT//fineness)] for i in range(WIDTH//fineness)])

    # breakpoint()

    training = True
    trial_counter = 0
    while training:
        step_counter = 0
        s = begin_pos
        old_Q = Q.copy()
        epsilon *= 0.99
        while step_counter < max_steps:
            a = get_action(Q, s, epsilon)
            new_s = (s[0] + MOVES[a][0], s[1] + MOVES[a][1])
            # r = -1 if new_s != end_pos else 0
            r = reward_getter(s[0], s[1])
            Q[s[0], s[1], a] = (1-alpha)*Q[s[0], s[1], a] + alpha*(r + gamma*max(Q[new_s[0], new_s[1]]))

            s = new_s

            step_counter += 1
            if r == 0:
                break


        trial_counter += 1
        if trial_counter % 100 == 0:
            print('Trial: {}, Delta: {}'.format(trial_counter, (abs(old_Q - Q)).sum()))
        if (abs(old_Q - Q)).sum() < threshold or trial_counter >= trials:
            training = False

    steps = 0
    s = begin_pos
    while steps < max_steps:
        a = get_action(Q, s, epsilon=0)

        new_s = (s[0] + MOVES[a][0], s[1] + MOVES[a][1])

        s = new_s
        steps += 1
        if s == end_pos:
            # print(steps)
            break

    print(steps)
    print(s)
    print(begin_pos)
    print(end_pos)

Q_Table()
