from pygame.draw import rect, circle, aaline, line
from pygame import Rect
from math import sqrt
from constants import BLUE, BLACK, screen, BEGINNING, END, RADIUS, GREEN

class Space():
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.rectangles = []
        self.circles = []
        circle(screen, BLUE, BEGINNING, RADIUS)
        circle(screen, BLUE, END, RADIUS)

    def add_rectangle(self, first, second):
        top_left = (min(first[0], second[0]), min(first[1], second[1]))
        bot_right = (max(first[0], second[0]), max(first[1], second[1]))
        self.rectangles.append((top_left, bot_right))
        left = top_left[0]
        top = top_left[1]
        w = bot_right[0] - top_left[0]
        h = bot_right[1] - top_left[1]
        rect(screen, BLACK, Rect(left, top, w, h))
        return

    def add_circle(self, first_point, second_point):
        center = (0.5*(first_point[0]+second_point[0]), 0.5*(first_point[1]+second_point[1]))
        radius = self.dist(center, first_point)
        self.circles.append((center, radius))
        circle(screen, BLACK, center, radius)
        return

    def in_obstacle(self, point):
        for top_left, bot_right in self.rectangles:
            if top_left[0] <= point[0] <= bot_right[0] and top_left[1] <= point[1] <= bot_right[1]:
                return True
        for center, radius in self.circles:
            if self.dist(point, center) <= radius:
                return True
        return False

    def is_legal(self, path, final_node):
        for i in range(len(path)-1):
            pos1 = path[i]
            pos2 = path[i+1]
            # for rectangles, we check if the line between pos1 and pos2 passes
            # through one of the 4 lines making the rectangle.
            for top_left, bot_right in self.rectangles:
                p00 = top_left
                p01 = (top_left[0], bot_right[1])
                p10 = (bot_right[0], top_left[1])
                p11 = bot_right
                intersecting = self._line_intersection_check(pos1, pos2, p00, p01)
                if intersecting:
                    return False
                intersecting = self._line_intersection_check(pos1, pos2, p00, p10)
                if intersecting:
                    return False
                intersecting = self._line_intersection_check(pos1, pos2, p11, p01)
                if intersecting:
                    return False
                intersecting = self._line_intersection_check(pos1, pos2, p11, p01)
                if intersecting:
                    return False

            # the strategy here is to find the point on the line(pos1, pos2) that
            # is closest to the center of the circle and test whether
            # that point is in the obstacle.
            # http://www.jeffreythompson.org/collision-detection/line-rect.php
            for center, radius in self.circles:
                closest_point = self._closest_point_on_line(pos1, pos2, center, check=True)
                if self.dist(center, closest_point) < radius:
                    return False

        return True

    def is_line_legal(self, pos1, pos2):
        for top_left, bot_right in self.rectangles:
            p00 = top_left
            p01 = (top_left[0], bot_right[1])
            p10 = (bot_right[0], top_left[1])
            p11 = bot_right
            intersecting = self._line_intersection_check(pos1, pos2, p00, p01)
            if intersecting:
                return False
            intersecting = self._line_intersection_check(pos1, pos2, p00, p10)
            if intersecting:
                return False
            intersecting = self._line_intersection_check(pos1, pos2, p11, p01)
            if intersecting:
                return False
            intersecting = self._line_intersection_check(pos1, pos2, p11, p01)
            if intersecting:
                return False
        for center, radius in self.circles:
            closest_point = self._closest_point_on_line(pos1, pos2, center, check=True)
            if self.dist(center, closest_point) < radius:
                return False
        return True

    def redraw(self):
        for (tlx,tly),(brx,bry) in self.rectangles:
            rect(screen, BLACK, Rect(tlx, tly, brx-tlx, bry-tly))
        for center, radius in self.circles:
            circle(screen, BLACK, center, radius)
        return

    def dist(self, pos, other):
        return sqrt((pos[0] - other[0])**2 + (pos[1] - other[1])**2)

    def draw_lines(self, path, final_node, color=BLACK, solution=False):
        for i in range(len(path)-1):
            point1 = path[i]
            point2 = path[i+1]
            if not solution:
                aaline(screen, color, point1, point2, blend=1)
            else:
                line(screen, color, point1, point2, width=3)
        point1 = path[-1]
        point2 = final_node
        if point2 != END:
            if not solution:
                aaline(screen, color, point1, point2, blend=1)
            else:
                line(screen, color, point1, point2, width=3)
            return

    def draw_line(self, pos1, pos2, color=BLACK):
        aaline(screen, color, pos1, pos2, blend=1)
        return

    def draw_astar_solution(self, pos1, pos2, color=GREEN, width=RADIUS):
        line(screen, color, pos1, pos2, width=width)
        self.draw_point(pos1, color=color, radius=RADIUS)

    def draw_point(self, pos, color=BLACK, radius=4):
        circle(screen, color, pos, radius)

    # https://stackoverflow.com/questions/47177493/python-point-on-a-line-closest-to-third-point
    # quicker than the standard (to me) calculus way
    def _closest_point_on_line(self, point1, point2, other_point, check=False):
        w1, h1 = point1
        w2, h2 = point2
        w3, h3 = other_point
        delta_w = w2-w1
        delta_h = h2-h1

        determinant = delta_w**2 + delta_h**2
        # if same point
        if determinant == 0:
            return point1

        a = (delta_h*(h3-h1) + delta_w*(w3-w1)) / determinant

        return w1 + a*delta_w, h1 + a*delta_h

    def _line_intersection_check(self, point1, point2, point3, point4):
        # point1 and point2 make one line, 3 and 4 the other
        if point1==point2 or point3==point4:
            return False
        uA = ((point4[0]-point3[0])*(point1[1]-point3[1]) - (point4[1]-point3[1])*(point1[0]-point3[0])) / \
            ((point4[1]-point3[1])*(point2[0]-point1[0]) - (point4[0]-point3[0])*(point2[1]-point1[1]));
        uB = ((point2[0]-point1[0])*(point1[1]-point3[1]) - (point2[1]-point1[1])*(point1[0]-point3[0])) / \
            ((point4[1]-point3[1])*(point2[0]-point1[0]) - (point4[0]-point3[0])*(point2[1]-point1[1]));

        if 0 <= uA <= 1 and 0 <= uB <= 1:
            return True
        return False
