from pygame import init
from pygame.display import set_mode

init()

# Define constants
WIDTH = 1280
HEIGHT = 720
BEGINNING = (WIDTH//10,HEIGHT//10)
END = (9*WIDTH//10,9*HEIGHT//10)
RADIUS = 8 # radius of points at beginning/end


# Define colors
WHITE = (255,255,255) # blank pixel
BLACK = (0,0,0) # background/wall
RED = (255,0,0) # closed set
GREEN = (0,255,0) # open set
BLUE = (0,0,255) # beginning/end
SKY_BLUE = (135, 206, 235) # best current guess for A* and D*

# # initialize background
screen = set_mode((WIDTH, HEIGHT))
screen.fill(WHITE)
