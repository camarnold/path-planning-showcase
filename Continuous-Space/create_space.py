from pygame.display import flip
from pygame.event import get
from pygame.mouse import get_pos, get_pressed
from pygame.locals import K_ESCAPE, KEYDOWN, QUIT, MOUSEBUTTONDOWN, K_SPACE, K_1, K_2, K_3, K_4, K_q, K_w, K_e, K_r
from constants import *
from classes import *


def Space_Maker(Obstacle_Space=None):

    if Obstacle_Space == None:
        Obstacle_Space = Space(WIDTH, HEIGHT)

    # helps create rectangles/circles
    first_click = None
    second_click = None
    center = None
    outer = None
    left_clicked = False
    right_clicked = False

    running = True
    been_pressed = False
    rectangle = True # whether to make a rectangle or circle
    while running:
        events = get()
        for event in events:
            if event.type == MOUSEBUTTONDOWN:
                if rectangle:
                    try:
                        pos = get_pos()
                        if not been_pressed:
                            been_pressed = True
                            first_click = pos
                        else:
                            been_pressed = False
                            second_click = pos
                            Obstacle_Space.add_rectangle(first_click, second_click)
                    except AttributeError:
                        pass
                else:
                    try:
                        pos = get_pos()
                        if not been_pressed:
                            been_pressed = True
                            first_point = pos
                        else:
                            been_pressed = False
                            second_point = pos
                            Obstacle_Space.add_circle(first_point, second_point)
                    except AttributeError:
                        pass

            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return False, Obstacle_Space, -1

                elif event.key == K_SPACE and not been_pressed:
                    rectangle = not rectangle

                elif event.key == K_1:
                    return True, Obstacle_Space, 1

                elif event.key == K_2:
                    return True, Obstacle_Space, 2

                elif event.key == K_3:
                    return True, Obstacle_Space, 3

                elif event.key == K_4:
                    return True, Obstacle_Space, 4

                # rooms
                elif event.key == K_q:
                    Obstacle_Space.rectangles += [((28, 10), (39, 570)), ((29, 11), (1010, 17)), ((1009, 12), (1012, 566)), ((30, 564), (1012, 566)), ((154, 14), (160, 122)), ((78, 113), (155, 116)), ((156, 117), (156, 310)), ((155, 116), (170, 296)), ((157, 14), (167, 118)), ((154, 345), (175, 564)), ((63, 475), (156, 480)), ((385, 414), (400, 488)), ((383, 95), (412, 157)), ((164, 78), (186, 141)), ((473, 11), (486, 153)), ((470, 463), (482, 569)), ((676, 15), (705, 132)), ((610, 98), (694, 127)), ((813, 326), (824, 563)), ((813, 111), (827, 275)), ((823, 113), (1011, 137)), ((814, 86), (825, 111)), ((812, 15), (823, 43)), ((537, 255), (815, 268)), ((470, 242), (483, 465)), ((166, 271), (239, 279)), ((390, 276), (470, 282))]
                    Obstacle_Space.circles += [((320.5, 131.0), 48.5), ((312.0, 463.0), 33.301651610693426)]
                    Obstacle_Space.redraw()

                # corridor
                elif event.key == K_w:
                    Obstacle_Space.rectangles += [((198, 28), (204, 542)), ((358, 55), (369, 562)), ((482, 10), (506, 510)), ((660, 72), (678, 560)), ((807, 11), (829, 482))]
                    Obstacle_Space.circles += [((199.0, 24.5), 17.00735135169495), ((202.0, 9.5), 20.006249023742555), ((362.0, 563.0), 15.033296378372908), ((494.0, 11.0), 26.019223662515376), ((661.5, 559.0), 20.59732992404598), ((817.5, 7.5), 20.65187642806338), ((110.0, 265.0), 16.278820596099706), ((282.5, 280.0), 12.539936203984453), ((423.5, 287.5), 14.577379737113251), ((590.5, 300.0), 15.628499608087784), ((742.0, 300.5), 15.20690632574555)]
                    Obstacle_Space.redraw()

                # 1 small door
                elif event.key == K_e:
                    Obstacle_Space.rectangles += [((451, 298), (463, 573)), ((451, 13), (464, 280))]
                    Obstacle_Space.redraw()

                # 2 small doors
                elif event.key == K_r:
                    Obstacle_Space.rectangles += [((350, 290), (376, 573)), ((346, 14), (378, 281)), ((634, 310), (666, 574)), ((631, 21), (666, 294))]
                    Obstacle_Space.redraw()

            elif event.type == QUIT:
                return False, Obstacle_Space, -1

        flip()

if __name__ == '__main__':
    Space_Maker()
