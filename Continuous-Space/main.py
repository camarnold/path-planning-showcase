from create_space import Space_Maker
from algorithms import *
from pygame.event import get
from pygame.locals import K_ESCAPE, KEYDOWN, QUIT, K_r

if __name__ == '__main__':
    keep_going = True
    area = None
    while keep_going:
        if area == None:
            keep_going, area, key = Space_Maker()
        else:
            keep_going, area, key = Space_Maker(area)
        if key == 1:
            RRT(area)
        elif key == 2:
            Probablistic_Roadmap(area)
        elif key == 3:
            Probablistic_Roadmap(area, new_node_selection='halton')
        elif key == 4:
            Probablistic_Roadmap(area, new_node_selection='halton_noise')
        events = get()
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    keep_going = False

                elif event.key == K_r:
                    keep_going, area, key = Space_Maker(area)

            elif event.type == QUIT:
                keep_going = False
