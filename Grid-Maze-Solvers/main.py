# https://realpython.com/pygame-a-primer/#basic-pygame-program

from pygame import init
from make_grid import *
from algorithms import *


if __name__ == '__main__':
    # Initialize pygame
    init()

    keep_going = True
    while keep_going:
        try:
            keep_going, alg, grid, start_pixel, end_pixel = Wall_Maker(grid)
        except:
            keep_going, alg, grid, start_pixel, end_pixel = Wall_Maker()
        if alg == 1:
            keep_going, grid = Ill_Informed_Alg(grid, start_pixel, end_pixel)
        elif alg == 2:
            keep_going, grid = Slightly_Less_Ill_Informed_Alg(grid, start_pixel, end_pixel)
        elif alg == 3:
            keep_going, grid = Dijkstras(grid, start_pixel, end_pixel)
        elif alg == 4:
            keep_going, grid = A_Star(grid, start_pixel, end_pixel)
